﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace LottoCalc
{
    internal class LottoCombination
    {
        public int AdditionalNumber { get; set; }

        public List<int> Combination { get; set; }

        /// <summary>
        /// combination's indesx in the FULL combination set (all possibilities)
        /// </summary>
        public int IndexInFullCombinationsSet { get; set; }

        [ScriptIgnore]
        public int PopulatedCount
        {
            get
            {
                return Combination.Count;
            }
        }

      

        public LottoCombination()
        {
            Combination = new List<int>();
        }



        public LottoCombination(int[] numbers, int additionalNumber)
        {
            Combination = new List<int>();

            foreach (int number in numbers)
            {
                Combination.Add(number);
            }

            AdditionalNumber = additionalNumber;
        }


        #region PUBLIC METHODS

        

        public void Add(int number)
        {
            Combination.Add(number);
        }

        /// <summary>
        /// prints combination + a message
        /// </summary>
        public void PrintWithMessage(string message)
        {
            string combinationInfo = GetCombinationInfo();
            Print(message, combinationInfo);
        }

        /// <summary>
        /// prints combination + its index
        /// </summary>
        /// <param name="index"></param>
        public void PrintWithIndex(int index)
        {
            PrintWithMessage(index.ToString());
        }


        public override string ToString()
        {
            return $"{Combination[0].ToString().PadLeft(2, ' ')}  {Combination[1].ToString().PadLeft(2, ' ')}" +
                $"  {Combination[2].ToString().PadLeft(2, ' ')}  {Combination[3].ToString().PadLeft(2, ' ')}  " +
                $"{Combination[4].ToString().PadLeft(2, ' ')}  {Combination[5].ToString().PadLeft(2, ' ')} ({AdditionalNumber})";
        }

        #endregion




        #region PRIVATE METHODS

        private void Print(string message, string combinationInfo)
        {
            Console.WriteLine("{0}. {1}", message, combinationInfo);
        }

        private string GetCombinationInfo()
        {
            StringBuilder sb = new StringBuilder();
            int i = 0;
            while (i < Combination.Count)
            {
                sb.Append(" ");
                sb.Append(Combination[i]);
                i++;
            }

            sb.Append(string.Format(" ({0})...............[index: {1}]", AdditionalNumber, IndexInFullCombinationsSet.ToString("N0")));

            return sb.ToString();
        }

        #endregion

    }
}
