﻿using LottoCalc.BL;
using LottoCalc.Helpers;
using LottoCalc.Optimizers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LottoCalc
{
    class Program
    {
        private static string _lastJsonSerializedCombinationsFilePath;
        private static string _lastReadableSerializedCombinationsFilePath;

        private static int _RunWholeSequence = 0;
        private static int _TestWinHistory = 1;
        private static int _CheckWin = 0;


        private static int ALGORYTHM = 1;
        
        private static List<Dictionary<string, float>> _winTableList;

        static void Main(string[] args)
        {
            if (_RunWholeSequence == 1)
            {
                RunWholeSequence();
            }

           
            if (_CheckWin == 1)
            {
                 // used for a specific run file
                string fileName = "2021.09.16-1444.txt";
                CheckWin($"{Config.FileFOlderPath}{fileName}", Config.WinningCombination, Config.WinningCombinationAdditionalNumber);
            }

            if (_TestWinHistory == 1)
            {
                TestWinHistory();
            }
                

            Console.WriteLine("**************   FINISHED **************");
            Console.ReadLine();
        }


        private static void TestWinHistory()
        {
            List<LottoCombination> winningCombinations = 
                CSVHelper.LoadCombinationFromCsv(@"..\..\..\last_1000.csv");

            // generate combinations for each past winning combination;
            // analyze win table
            _winTableList = new List<Dictionary<string, float>>();
            foreach(LottoCombination winningCombination in winningCombinations)
            {
                List<LottoCombination> combinations = GenerateCombinations();
                CheckWin(combinations, winningCombination.Combination.ToArray(), winningCombination.AdditionalNumber);
            }


            Console.WriteLine($"\n\n *** AVARAGE WIN TABLE AFTER GENERATING [{Config.NumberOfRowsToSelect}] ROWS;" +
                $" RUNNING ON [{winningCombinations.Count}] WINNING COMBINATIONS:\n");

            Engine.AnalyzeWinTableList(_winTableList);
        }



        /// <summary>
        ///  calc combinations, serialize to file, calc stats, check win
        /// </summary>
        private static void RunWholeSequence()
        {
            //
            //  CALULATE COMBINATIONS
            List<LottoCombination> combinations = GenerateCombinations();

            // readable file
            SaveCombinationsToLottoFormFormat(combinations);

            // stats
            ConcurrentDictionary<int, int> numberStats = Engine.GetNumberDistribution(combinations);
            Dictionary<int, int> statsToDictionary = numberStats.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            Logger.PrintsDigitStats(statsToDictionary);
            PersistanceHandler.AppendNumberStatsToFile(_lastReadableSerializedCombinationsFilePath, statsToDictionary);

            // upper/lower range stats
            Dictionary<string, KeyValuePair<int, int>> lowerUpperRange = Engine.GetStatsUpperAndLowerRange(statsToDictionary);
            string minNumOccuranceMsg = $"Min Occurance of {lowerUpperRange["minimum"].Key} ({lowerUpperRange["minimum"].Value}) ";
            string maxNumOccuranceMsg = $"Max Occurance of {lowerUpperRange["maximum"].Key} ({lowerUpperRange["maximum"].Value}) ";
            PersistanceHandler.AppendTextToFile(_lastReadableSerializedCombinationsFilePath, minNumOccuranceMsg);
            PersistanceHandler.AppendTextToFile(_lastReadableSerializedCombinationsFilePath, maxNumOccuranceMsg);
            Console.WriteLine(minNumOccuranceMsg);
            Console.WriteLine(maxNumOccuranceMsg);

            // win
            CheckWin(_lastJsonSerializedCombinationsFilePath, Config.WinningCombination, Config.WinningCombinationAdditionalNumber);
        }


        /// <summary>
        /// saves the combonation to a readable lotto form format
        /// </summary>
        private static void SaveCombinationsToLottoFormFormat(List<LottoCombination> combinations)
        {
            string targetFileName = $"{Config.FileFOlderPath}{DateTime.Now.ToString("yyyy.MM.dd-HHmm")}-FORMS.txt".Replace(@"\\", @"\");
            _lastReadableSerializedCombinationsFilePath = targetFileName;
            PersistanceHandler.SaveCombinationsToReadableFormat(combinations, targetFileName);
        }



        /// <summary>
        /// generates combinations accordin to config; serialize them to a file
        /// </summary>
        private static List<LottoCombination> GenerateCombinations()
        {
            DateTime startTime = DateTime.Now;

            // get selected combinations
            List<LottoCombination> selectedLotteryCombinations;
            if (ALGORYTHM == 1)
            {
                selectedLotteryCombinations = Engine.GetLotteryCombinations();
            }
            else
            {
                selectedLotteryCombinations = RowsGenerator.GetLotteryCombinations2();
            }

          
             Logger.PrintAll(selectedLotteryCombinations);
           

            //
            // serialize
            string targetFileName = $"{Config.FileFOlderPath}{DateTime.Now.ToString("yyyy.MM.dd-HHmm")}.txt".Replace(@"\\", @"\");
            PersistanceHandler.SaveCombinationsToFile(selectedLotteryCombinations, targetFileName);
            _lastJsonSerializedCombinationsFilePath = targetFileName;

            // take time
            DateTime endTime = DateTime.Now;
            TimeSpan runTime = endTime - startTime;
         //   Console.WriteLine("RunTime: {0}\n", runTime.Seconds);

            return selectedLotteryCombinations;
        }



        private static void CheckWin(string combinationsFilePath, int[] winningNumbers, int additionalNumber)
        {
            // load saved combinations
            List<LottoCombination> selectedLotteryCombinations = PersistanceHandler.LoadCombinaionsFromFile(combinationsFilePath);

            CheckWin(selectedLotteryCombinations, winningNumbers, additionalNumber);
        }



        private static void CheckWin( List<LottoCombination> selectedLotteryCombinations, int[] winningNumbers, int additionalNumber)
        {
            LottoCombination winningCombination = new LottoCombination()
            {
                Combination = winningNumbers.ToList(),
                AdditionalNumber = additionalNumber
            };

            // check
            Dictionary<string, float> winTable =  Engine.CheckWin(selectedLotteryCombinations, winningCombination);
            if (_TestWinHistory == 1)
            {
                _winTableList.Add(winTable);
            }
        }


        #region old


        private static void Test1()
        {
            List<LottoCombination> selectedCombinations = new List<LottoCombination>();
            selectedCombinations.Add(new LottoCombination()
            {
                Combination = (new int[] { 2, 4, 8, 9, 16, 18 }).ToList(), // 3,3+,4,4+
                AdditionalNumber = 5
            });
            selectedCombinations.Add(new LottoCombination()
            {
                Combination = (new int[] { 2, 4, 7, 14, 18, 22 }).ToList(), // --
                AdditionalNumber = 2
            });
            selectedCombinations.Add(new LottoCombination()
            {
                Combination = (new int[] { 11, 16, 18, 21, 22, 23 }).ToList(), // 3
                AdditionalNumber = 7
            });
            selectedCombinations.Add(new LottoCombination()
            {
                Combination = (new int[] { 1, 4, 8, 11, 16, 18 }).ToList(), // 3,4,5,6
                AdditionalNumber = 7
            });

            //
            //
            LottoCombination winningComb = new LottoCombination()
            {
                Combination = (new int[] { 1, 4, 8, 11, 16, 18 }).ToList(),
                AdditionalNumber = 5
            };
            RowsGenerator.CheckWin(selectedCombinations, winningComb);

        }





        /// <summary>
        /// dynamic calculation
        /// </summary>
        private static void POC2()
        {
            int n = 5;
            int k = 3;

            int currentPosVal = 1;

            List<LottoCombination> allCombinations = new List<LottoCombination>();
            DynamicGeneration(n, k, 1, currentPosVal, null, allCombinations);

            Logger.PrintAll(allCombinations);
        }



        private static void DynamicGeneration(int n, int k, int position, int currentPosVal,
                                              LottoCombination currentCombination, List<LottoCombination> allCombination)
        {
            if (position == k)
            {
                allCombination.Add(currentCombination);
                return;
            }
            if (currentPosVal <= (n - k + currentPosVal))
            {
                if (currentCombination == null)
                {
                    // first number in combination
                    currentCombination = new LottoCombination();
                }
                currentCombination.Add(currentPosVal);
                DynamicGeneration(n, k, ++position, ++currentPosVal, currentCombination, allCombination);

                // after recursion return
                if (position != 1)
                {
                    // strat from 1st position again
                    return;
                }
                DynamicGeneration(n, k, position, ++currentPosVal, null, allCombination);
            }
        }



        #endregion


    }
}
