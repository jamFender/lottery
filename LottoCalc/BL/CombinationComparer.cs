﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LottoCalc.BL
{
    internal class CombinationComparer : IComparer<LottoCombination>
    {
        public int Compare(LottoCombination x, LottoCombination y)
        {
            if (x.Combination[0] > y.Combination[0])
            {
                return 1;
            }
            if (x.Combination[0] < y.Combination[0])
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
