﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace LottoCalc.BL
{
    internal class PersistanceHandler
    {
        #region PUBLIC METHODS


        public static void SaveCombinationsToFile(List<LottoCombination> combinations, string targetFileName)
        {
            // serialize
            string json = SerializeCombinationsToJson(combinations);

            // save to file
            using (StreamWriter file = File.CreateText($@"{targetFileName}"))
            {
                file.Write(json);
            }
        }





        internal static List<LottoCombination> LoadCombinaionsFromFile(string combinationsFilePath)
        {
            string combinationsJson;

            // read from file
            using (StreamReader readtext = new StreamReader(combinationsFilePath))
            {
                combinationsJson = readtext.ReadLine();
            }

            // deserialize
            return DeSerializeCombinationsFromJson(combinationsJson);
        }


        /// <summary>
        /// divides and saves the combination to lotto forms (14 combinations per form)
        /// </summary>
        internal static void SaveCombinationsToReadableFormat(List<LottoCombination> combinations, string targetFileName)
        {
            // save to file
            using (StreamWriter file = File.CreateText($@"{targetFileName}"))
            {
                int formCounter = 1;
                int combinationIdx = 0;
                while (true)
                {
                    file.WriteLine($"\n\n........................ Form # {formCounter++} ........................");
                    int countPerForm = 0;
                    while ((countPerForm < Config.CombinationsPerForm) && (combinationIdx < combinations.Count))
                    {
                        file.WriteLine(combinations[combinationIdx]);
                        combinationIdx++;
                        countPerForm++;
                    }

                    if (combinationIdx == combinations.Count)
                    {
                        // no more combinations
                        break;
                    }
                }
            }
        }



        internal static void AppendNumberStatsToFile(string targetFileName, Dictionary<int, int> numberSelectionStats)
        {
            using (FileStream fileStream = new FileStream(targetFileName, FileMode.Append, FileAccess.Write))
            using (StreamWriter streamWriter = new StreamWriter(fileStream))
            {
                streamWriter.WriteLine("\n\n\n\n DIGITS DISTRIBUTION \n");
                foreach (int key in numberSelectionStats.Keys)
                {
                    streamWriter.WriteLine($"[{key.ToString().PadLeft(2, '0')}] => ({numberSelectionStats[key].ToString().PadLeft(3, ' ')}) {new string('.', numberSelectionStats[key])}");
                }
            }
        }



        internal static void AppendTextToFile(string targetFileName, string text)
        {
            using (FileStream fileStream = new FileStream(targetFileName, FileMode.Append, FileAccess.Write))
            using (StreamWriter streamWriter = new StreamWriter(fileStream))
            {
                streamWriter.WriteLine($"\n{text}\n");
            }
        }
        #endregion



        #region PRIVATE METHODS

        private static string SerializeCombinationsToJson(List<LottoCombination> combinations)
        {
            string json = new JavaScriptSerializer().Serialize(combinations);
         //   Console.WriteLine($"\nSerialized--->\n{json}");

            return json;
        }



        private static List<LottoCombination> DeSerializeCombinationsFromJson(string serialzedJson)
        {
            return (List<LottoCombination>)new JavaScriptSerializer().Deserialize(serialzedJson, typeof(List<LottoCombination>));
        }


        #endregion

    }
}
