﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LottoCalc.BL
{
    internal static class ExtensionMethods
    {
        /// <summary>
        /// return true if 'otherCombination' has more than X common numbers with
        /// 'currentCombination'
        /// </summary>
        /// <param name="currentCombination"></param>
        /// <param name="otherCombination"></param>
        /// <returns></returns>
        internal static bool HasCommonNumbers(this LottoCombination currentCombination,
                                                LottoCombination otherCombination)
        {
            if(ReferenceEquals(currentCombination, otherCombination))
            {
                return true;
            }
            
            //
            // check for common numbers with othe combination
            int commonNumbers = 0;
            foreach(int n in otherCombination.Combination)
            {
                if(currentCombination.Combination.Contains(n))
                {
                    commonNumbers++;
                }
            }

            if(commonNumbers > Config.MaxOverlapSubCombinations)
            {
                return true;
            }

            return false;
        }



        internal static void SetWinningStatus(this LottoCombination winningCombination,
                                                LottoCombination combinationInQuestion,
                                                Dictionary<string, float> winTable)
        {
            // additional
            bool additionalNumberIndication = false;
            if (winningCombination.AdditionalNumber == combinationInQuestion.AdditionalNumber)
            {
                additionalNumberIndication = true;
            }

            // numbers
            int commonNumbers = 0;
            foreach (int n in combinationInQuestion.Combination)
            {
                if (winningCombination.Combination.Contains(n))
                {
                    commonNumbers++;
                    if(commonNumbers >=3)
                    {
                        string winKey = commonNumbers.ToString();
                        float newWinCount =  (winTable[winKey] + 1);
                        winTable[winKey] =  newWinCount;
                        // 
                        if (additionalNumberIndication)
                        {
                            winKey += "+";
                            newWinCount = (winTable[winKey] + 1);
                            winTable[winKey] = newWinCount;
                        }
                    }
                }
            }
        }
    }
}
