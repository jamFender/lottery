﻿using LottoCalc.Helpers;
using LottoCalc.Optimizers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace LottoCalc.BL
{
    internal class Engine
    {
        #region BATCH MEMBERS

        static List<LottoCombination> _historicalWinningCombinations;
        static List<LottoCombination> _allCombinations;

        #endregion


        
        #region PUBLIC METHODS

        /// <summary>
        ///  retruns 'randomRowsToChoose' lottery combinations
        /// </summary>
        internal static List<LottoCombination> GetLotteryCombinations()
        {
            //
            //  generate all rows
            //
            int n = Config.TotalNumbers_N;
            int k = Config.Choose_K; ;
            int maxAdditionalNumbers = Config.NumAdditionalNumbers;   //7;

            if (_allCombinations == null)
            {
                _allCombinations = RowsGenerator.GenerateAllRows(n, k, maxAdditionalNumbers);
                Console.WriteLine("#############################\n\n number of combinations: {0}\n", _allCombinations.Count.ToString("N0"));
                Console.WriteLine(" winning odds: 1:{0}\n ############################# \n\n", (_allCombinations.Count / Config.NumberOfRowsToSelect).ToString("N0"));
            }
            //
            // add optimizers
            //  RowsGenerator.AddOptimizer(new CommonCombinationsDissmiserOptimizer());
            RowsGenerator.AddOptimizer(new SortOptimizer());

            // TODO:add validators...........

            //
            //  random rows to choose
            //
            LottoCombination[] lottoCombinationsArray = new LottoCombination[_allCombinations.Count];
            _allCombinations.CopyTo(lottoCombinationsArray);
            List<LottoCombination> selectedCombinations =
                                RowsGenerator.GetActualRowSet(lottoCombinationsArray.ToList());

            return selectedCombinations;
        }




        /// <summary>
        /// given a set of combinations, checks if a specified combination satisfies a win;
        /// returns the winning-table
        /// </summary>
        internal static Dictionary<string, float> CheckWin(List<LottoCombination> selectedCombinations, LottoCombination winningCombination)
        {
            Dictionary<string, float> winTable = RowsGenerator.CheckWin(selectedCombinations, winningCombination);
         

            // print
            Logger.PrinWinTable(winTable);
            Logger.PrintWinValue(winTable);

            return winTable;
        }



        internal static void RunBatch()
        {
            //
            //  generate all rows
            //
            int n = Config.TotalNumbers_N;
            int k = Config.Choose_K;
            int maxAdditionalNumbers = Config.NumAdditionalNumbers;

            //
            // add optimizers
            RowsGenerator.AddOptimizer(new SortOptimizer());

            // TODO:add validators...........

            //
            // Run
            DateTime startTime = DateTime.Now;
            _allCombinations = RowsGenerator.GenerateAllRows(n, k, maxAdditionalNumbers);
            Console.WriteLine("# of combinations: {0}\n", _allCombinations.Count.ToString("N0"));

            // read csv
            ReadCsvFile();
        }


        /// <summary>
        /// reurns distribution of numbers in all given combinations
        /// </summary>
        internal static ConcurrentDictionary<int, int> GetNumberDistribution(List<LottoCombination> combinations)
        {
            ConcurrentDictionary<int, int> numberSelectionHash = new ConcurrentDictionary<int, int>();
            foreach(LottoCombination combination in combinations)
            {
                RowsGenerator.AddNumberStats(combination, numberSelectionHash);
            }

            return numberSelectionHash;
        }



        internal static Dictionary<string, KeyValuePair<int, int>> GetStatsUpperAndLowerRange(Dictionary<int, int> stats)
        {
            Dictionary<string, KeyValuePair<int, int>> res = new Dictionary<string, KeyValuePair<int, int>>();

            KeyValuePair<int, int> lowerRangeOccurance = stats.OrderBy(kvp => kvp.Value).First();
            res.Add("minimum", lowerRangeOccurance);

            KeyValuePair<int, int> upperRangeOccurance = stats.OrderBy(kvp => kvp.Value).Last();
            res.Add("maximum", upperRangeOccurance);
            return res;
            //res.Add();
        }


        internal static void AnalyzeWinTableList(List<Dictionary<string, float>> winTableList)
        {
            Dictionary<string, float> winTablesAvarage = RowsGenerator.InitWinTable();
            foreach (Dictionary<string, float> winTable in winTableList)
            {
                // sum all
                winTablesAvarage["3"] += winTable["3"];
                winTablesAvarage["3+"] += winTable["3+"];
                winTablesAvarage["4"] += winTable["4"];
                winTablesAvarage["4+"] += winTable["4+"];
                winTablesAvarage["5"] += winTable["5"];
                winTablesAvarage["5+"] += winTable["5+"];
                winTablesAvarage["6"] += winTable["6"];
                winTablesAvarage["6+"] += winTable["6+"];
            }
            Logger.PrintConfig();

            // Totals
            Logger.PrinWinTable(winTablesAvarage);

            
            // Avarage
            winTablesAvarage["3"] = winTablesAvarage["3"] / winTableList.Count;
            winTablesAvarage["3+"] = winTablesAvarage["3+"] / winTableList.Count;
            winTablesAvarage["4"] = winTablesAvarage["4"] / winTableList.Count;
            winTablesAvarage["4+"] = winTablesAvarage["4+"] / winTableList.Count;
            winTablesAvarage["5"] = winTablesAvarage["5"] / winTableList.Count;
            winTablesAvarage["5+"] = winTablesAvarage["5+"] / winTableList.Count;
            winTablesAvarage["6"] = winTablesAvarage["6"] / winTableList.Count;
            winTablesAvarage["6+"] = winTablesAvarage["6+"] / winTableList.Count;
          
            Logger.PrinWinTable(winTablesAvarage);
            Logger.PrintWinValue(winTablesAvarage);
        }
        #endregion



        #region PRIVATE METHODS


        private static void ReadCsvFile()
        {
            _historicalWinningCombinations = new List<LottoCombination>();

            using (var reader = new StreamReader(@"C:\test.csv"))
            {
                List<string> listA = new List<string>();
                List<string> listB = new List<string>();
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] stringValues = line.Split(',');
                    List<int> intValues = new List<int>();
                    for (int i = 0; i < 6; i++)
                    {
                        intValues.Add(Int32.Parse(stringValues[i]));
                    }
                    int additionalNumber = Int32.Parse(stringValues[6]);

                    LottoCombination winnigComb = new LottoCombination(intValues.ToArray(), additionalNumber);
                    _historicalWinningCombinations.Add(winnigComb);
                }
            }
        }

        #endregion

    }
}
