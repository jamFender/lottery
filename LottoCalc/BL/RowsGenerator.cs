﻿using LottoCalc.Helpers;
using LottoCalc.Optimizers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LottoCalc.BL
{
    internal class RowsGenerator
    {
        #region MEMBERS

        static List<ILottoSetOptimizer> _optimizers;

        static Random _random = new Random();
        
        #endregion



        #region INIT

        /// <summary>
        /// adds an optimzer to list
        /// </summary>
        /// <param name="optimizer"></param>
        internal static void AddOptimizer(ILottoSetOptimizer optimizer)
        {
            if (_optimizers == null)
            {
                _optimizers = new List<ILottoSetOptimizer>();
            }

            _optimizers.Add(optimizer);
        }

        #endregion



        #region ALGORITHM 1



        /// <summary>
        /// choose k from n
        /// </summary>
        /// <param name="n">total numbers to choose from</param>
        /// <param name="k">choose</param>
        internal static List<LottoCombination> GenerateAllRows(int n, int k, int maxAdditionalNumbers)
        {
            int pos1 = 1;
            int pos2 = 2;
            int pos3 = 3;
            int pos4 = 4;
            int pos5 = 5;
            int pos6 = 6;

            List<LottoCombination> allCombination = new List<LottoCombination>();

            for (int valPos1 = 1; valPos1 <= (n - k + pos1); valPos1++) // pos1
            {
                for (int valPos2 = valPos1 + 1; valPos2 <= (n - k + pos2); valPos2++) // pos2
                {
                    for (int valPos3 = valPos2 + 1; valPos3 <= (n - k + pos3); valPos3++) // pos3
                    {
                        for (int valPos4 = valPos3 + 1; valPos4 <= (n - k + pos4); valPos4++) // pos4
                        {
                            for (int valPos5 = valPos4 + 1; valPos5 <= (n - k + pos5); valPos5++) // pos5
                            {
                                for (int valPos6 = valPos5 + 1; valPos6 <= (n - k + pos6); valPos6++) // pos6
                                {
                                    int additionalNumber = 1;
                                    while (additionalNumber <= maxAdditionalNumbers)
                                    {
                                        LottoCombination combination = new LottoCombination(new int[] { valPos1, valPos2, valPos3, valPos4, valPos5, valPos6 }, additionalNumber);
                                        additionalNumber++;
                                        allCombination.Add(combination);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return allCombination;
        }



        #endregion


        #region ALGORITHM 2

        /// <summary>
        /// instead of calculating ALL options, choose every time from scratch; eliminate duplications
        /// </summary>
        /// <returns></returns>
        internal static List<LottoCombination> GetLotteryCombinations2()
        {
            int n = Config.TotalNumbers_N;
            int k = Config.Choose_K; ;

            List<LottoCombination> selectedCombinations = new List<LottoCombination>();

            int indexToSelect;

            int combinationCount = 0;
            while (combinationCount < Config.NumberOfRowsToSelect)
            {
                var range = Enumerable.Range(1, n).ToList();
                var exclude = new HashSet<int>();
                LottoCombination currentCombination = new LottoCombination();
                while (currentCombination.Combination.Count < k)
                {
                    indexToSelect = _random.Next(0, n - exclude.Count);
                    int selectedNumber = range[indexToSelect];
                    currentCombination.Combination.Add(selectedNumber);
                    exclude.Add(selectedNumber);
                    range.Remove(selectedNumber);
                }

                // check duplicated numbers
                if (selectedCombinations.Any(comb => comb.HasCommonNumbers(currentCombination)))
                {
                    // more than allowed common numbers was found - discard index and find a new one
                    Logger.CommonCombinationFound(currentCombination);
                }
                else
                {
                    currentCombination.AdditionalNumber = _random.Next(1, Config.NumAdditionalNumbers);
                    currentCombination.Combination.Sort();
                    selectedCombinations.Add(currentCombination);

                    combinationCount++;
                }
            }

            selectedCombinations.Sort(new CombinationComparer());
            return selectedCombinations;
        }

        #endregion

        internal static Dictionary<string, float> CheckWin(List<LottoCombination> selectedCombinations, LottoCombination winningCombination)
        {
            Dictionary<string, float> winTable = InitWinTable();


            foreach(LottoCombination combination in selectedCombinations)
            {
                winningCombination.SetWinningStatus(combination, winTable);
            }

            return winTable;
        }

      

        internal static Dictionary<string, float> InitWinTable()
        {
            Dictionary<string, float> winTable = new Dictionary<string, float>();
            winTable.Add("3", 0);
            winTable.Add("3+", 0);
            winTable.Add("4", 0);
            winTable.Add("4+", 0);
            winTable.Add("5", 0);
            winTable.Add("5+", 0);
            winTable.Add("6", 0);
            winTable.Add("6+", 0);

            return winTable;

        }

        #region UTILITY METHODS


        #endregion


        #region PRIVATE METHODS


        /// <summary>
        /// returns 'numOfRowsToChoose' row indexes from 'allCombination' set
        /// </summary>
        /// <param name="allCombinations"></param>
        /// <param name="numOfRowsToChoose"></param>
        /// <returns></returns>
        private static List<int> GetRandomRowIndexes(List<LottoCombination> allCombinations, int numOfRowsToChoose)
        {
            int minRange = 1;
            int maxRange = allCombinations.Count;
            List<int> selectedRowsIndex = new List<int>();

            //
            // get random indexes
            int i = 0;
            while (i < numOfRowsToChoose)
            {
                int randomNum = _random.Next(minRange, maxRange);
                if (!selectedRowsIndex.Contains(randomNum))
                {
                    selectedRowsIndex.Add(randomNum);
                    i++;
                }
            }

            return selectedRowsIndex;
        }



        /// <summary>
        /// returns the actual desired row set
        /// </summary>
        /// <param name="allCombinations">all row combinations</param>
        /// <returns></returns>
        internal static List<LottoCombination> GetActualRowSet(List<LottoCombination> allCombinations)
        {
            List<LottoCombination> selectedCombinations = new List<LottoCombination>();
            while(selectedCombinations.Count < Config.NumberOfRowsToSelect)
            {
                int newCombinationIndex = GenerateNewCombinationIndex(allCombinations.Count);
                AddCombinationToSelection(allCombinations, newCombinationIndex, selectedCombinations);
            }
            

            // optimize
            foreach(ILottoSetOptimizer optimizer in _optimizers)
            {
                selectedCombinations = optimizer.OptimizeSet(allCombinations, selectedCombinations);
            }

            return selectedCombinations;
        }



        /// <summary>
        /// tries to add a combination to combination-set. If combination holds
        /// more than allowed common numbers, discards current combination and finds a new one
        /// </summary>
        /// <param name="allCombinations"></param>
        /// <param name="index"></param>
        /// <param name="selectedCombinations"></param>
        private static void AddCombinationToSelection(List<LottoCombination> allCombinations, int index,
                                                    List<LottoCombination> selectedCombinations)
        {
            LottoCombination combinationToAdd = allCombinations[index];

            if (selectedCombinations.Any(comb => comb.HasCommonNumbers(combinationToAdd)))
            {
                // more than allowed common numbers was found - discard index and find a new one
                Logger.CommonCombinationFound(allCombinations[index]);
                // remove not allowed combination 
                allCombinations.RemoveAt(index);

                while (true)
                {
                    int newCombinationIndex = GenerateNewCombinationIndex(allCombinations.Count);
                    LottoCombination newCombination = allCombinations[newCombinationIndex];
                    newCombination.IndexInFullCombinationsSet = newCombinationIndex;
                    if (selectedCombinations.Any(comb => comb.HasCommonNumbers(newCombination)))
                    {
                        // remove not allowed combination
                        allCombinations.RemoveAt(newCombinationIndex);
                        continue;
                    }
                    else
                    {
                        selectedCombinations.Add(newCombination);
                        break;
                    }
                }
            }
            else
            {
                // can add
                combinationToAdd.IndexInFullCombinationsSet = index;
                selectedCombinations.Add(combinationToAdd);
            }
        }

      
        private static int GenerateNewCombinationIndex(int allCombinationsCount)
        {
            int minRange = 1;
            int maxRange = allCombinationsCount;

            //
            // get random index
            int randomIndex = _random.Next(minRange, maxRange);
            return randomIndex;
        }



        /// <summary>
        /// increments the value of a number's entry in stats dictionary
        /// </summary>
        /// <param name="newCombination"></param>
        /// <param name="numberSelectionStats"></param>
        internal static void AddNumberStats(LottoCombination newCombination,
                                           ConcurrentDictionary<int, int> numberSelectionStats)
        {
            for(int i = 0; i <  newCombination.Combination.Count; i++)
            {
                // 1 = first add
                numberSelectionStats.AddOrUpdate(newCombination.Combination[i], 1,
                                                 (x,y) => numberSelectionStats[newCombination.Combination[i]] + 1);
            }
        }

      


        #endregion

    }
}
