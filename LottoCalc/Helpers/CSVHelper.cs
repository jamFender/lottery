﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LottoCalc.Helpers
{
    internal class CSVHelper
    {
        internal static List<LottoCombination> LoadCombinationFromCsv(string filePath)
        {
            var combinations = new List<LottoCombination>();

            using (var reader = new StreamReader(filePath))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    LottoCombination combination = new LottoCombination()
                    {
                        Combination = new List<int>() { Int32.Parse(values[0]),
                            Int32.Parse(values[1]),
                            Int32.Parse(values[2]),
                            Int32.Parse(values[3]),
                            Int32.Parse(values[4]),
                            Int32.Parse(values[5])
                        },
                        AdditionalNumber = Int32.Parse(values[6])
                    };

                    combinations.Add(combination);
                }
            }

            return combinations;
        }
    }
}



