﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LottoCalc.Helpers
{
    internal class Logger
    {
        internal static void PrintAll(List<LottoCombination> allCombination, int? printCount = null)
        {
            if (Config.LogLevel == "ALL")
            {
                int count = 1;
                foreach (var combination in allCombination)
                {
                    combination.PrintWithIndex(count);
                    count++;

                    if (printCount != null)
                    {
                        if (count == printCount)
                        {
                            break;
                        }
                    }
                }
            }
        }


        internal static void CommonCombinationFound(LottoCombination combination)
        {
            if (Config.LogLevel == "ALL")
            {
                combination.PrintWithMessage("Common Combination Was Found");
            }
        }



        internal static void PrintsDigitStats(Dictionary<int, int> numberSelectionStats)
        {
            if (Config.LogLevel == "ALL")
            {
                Console.WriteLine("\n\n DIGITS DISTRIBUTION \n");
                foreach (int key in numberSelectionStats.Keys)
                {
                    Console.WriteLine($"[{key.ToString().PadLeft(2, '0')}] => ({numberSelectionStats[key].ToString().PadLeft(3, ' ')}) {new string('.', numberSelectionStats[key])}");
                }
            }
        }



        internal static void PrintWinValue(Dictionary<string, float> winTable)
        {
            if (Config.LogLevel == "ALL" || Config.LogLevel == "PART")
            {
                float winValue = 0;

                Console.WriteLine("\n>> Money invested {0} NIS\n", Config.NumberOfRowsToSelect * 3);

                // print lower-range
                foreach (string key in winTable.Keys)
                {
                    winValue += Config.LowerRangeWinValue[key] * winTable[key];
                }
                Console.WriteLine("\n>> Lower Range Win Value (up to '5+'): {0} NIS\n", winValue);

                // print upper-range
                foreach (string key in winTable.Keys)
                {
                    winValue += Config.UpperRangeWinValue[key] * winTable[key];
                }
                Console.WriteLine("\n>> Upper Range Win Value (up to '5+'): {0} NIS\n", winValue);

            }
        }

        internal static void PrinWinTable(Dictionary<string, float> winTable)
        {
            if (Config.LogLevel == "ALL" || Config.LogLevel == "PART")
            {
                Console.WriteLine("\nWinning Status:");

                foreach (string key in winTable.Keys)
                {
                    Console.WriteLine("{0} : {1}", key, winTable[key]);
                }
            }
        }


        internal static void PrintConfig()
        {
            if (Config.LogLevel == "ALL" || Config.LogLevel == "PART")
            {
                Console.WriteLine($"--- CONFIG ---\n# of generated rows: [{Config.NumberOfRowsToSelect}]\n" +
                                  $"max common sub-combination: [{Config.MaxOverlapSubCombinations}]");
            }
        }
    }

}
