﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LottoCalc.Optimizers
{
    internal class SortOptimizer : ILottoSetOptimizer
    {
        /// <summary>
        /// orders set by its first element
        /// </summary>
        /// <param name="allCombinations"></param>
        /// <param name="selectedCombinations"></param>
        /// <returns></returns>
        public List<LottoCombination> OptimizeSet(List<LottoCombination> allCombinations, List<LottoCombination> selectedCombinations)
        {
            return selectedCombinations.OrderBy(comb => comb.IndexInFullCombinationsSet).ToList(); ;
        }
    }
}
