﻿using LottoCalc.BL;
using LottoCalc.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LottoCalc.Optimizers
{
    internal class CommonCombinationsDissmiserOptimizer : ILottoSetOptimizer
    {
        static Random _random = new Random();

        public List<LottoCombination> OptimizeSet(List<LottoCombination> allCombinations,
                                                    List<LottoCombination> selectedCombinations)
        {
            for(int i = 0; i < selectedCombinations.Count; i++)
            {
                if (selectedCombinations.Any(comb => comb.HasCommonNumbers(selectedCombinations[i])))
                {
                    Logger.CommonCombinationFound(selectedCombinations[i]);
                    Logger.PrintAll(selectedCombinations); // TODO: remove this
                    Console.WriteLine("\n"); // TODO: remove this

                    // more than allowed common numbers was found - 
                    // remove it, and generate a new one that holds the criteria
                    selectedCombinations.RemoveAt(i);
                    while (true)
                    {
                        LottoCombination newCombination = GenerateNewCombination(allCombinations);
                        if (selectedCombinations.Any(comb => comb.HasCommonNumbers(newCombination)))
                        {
                            continue;
                        }
                        else
                        {
                            selectedCombinations.Add(newCombination);
                            break;
                        }
                    }
                }
            }

            return selectedCombinations;
        }



        private LottoCombination GenerateNewCombination(List<LottoCombination> allCombinations)
        {
            int minRange = 1;
            int maxRange = allCombinations.Count;
            List<int> selectedRowsIndex = new List<int>();

            //
            // get random indexe
            int randomNum = _random.Next(minRange, maxRange);
            return allCombinations[randomNum];
        }
    }
}
