﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LottoCalc.Optimizers
{
    internal interface ILottoSetOptimizer
    {
        /// <summary>
        /// given all combinations, and a selected set, returns an optimized List<LottoCombination>
        /// </summary>
        /// <param name="allCombinations"></param>
        /// <param name="selectedCombinations"></param>
        List<LottoCombination> OptimizeSet(List<LottoCombination> allCombinations, List<LottoCombination> selectedCombinations);
    }
}
