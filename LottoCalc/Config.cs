﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LottoCalc
{
    internal class Config
    {
        internal static int TotalNumbers_N = 37;
        internal static int Choose_K = 6;
        internal static int NumAdditionalNumbers = 7;

        /// <summary>
        /// max overlapping sub-combinations in all sets
        /// </summary>
        internal static int MaxOverlapSubCombinations = 4;
        
        internal static int NumberOfRowsToSelect = 400;

        internal static string FileFOlderPath = $@"{Directory.GetCurrentDirectory()}\..\..\..\SaveCombinations\";

        public static int CombinationsPerForm = 14;

        public static string LogLevel = "PART"; // MIN; PART; ALL 

        //
        // Winning Combination
        internal static int[] WinningCombination = new int[] { 9, 17, 18, 23, 25, 36 }; //  
        internal static int WinningCombinationAdditionalNumber = 3;


        /*
         *          win range NIS:
         *                  3           10      15
         *                  3+          35      40
         *                  4           45      60
         *                  4+          150     200
         *                  5           600     850
         *                  5+          3000    7000
         *                  6
         *                  6+
         * 
         * */
        internal static Dictionary<string, int> LowerRangeWinValue = new Dictionary<string, int>()
        {
            {"3",10 }, {"3+",40}, {"4",60}, {"4+",150}, {"5",600}, {"5+",3000}, {"6",0}, {"6+",0}
        };

        internal static Dictionary<string, int> UpperRangeWinValue = new Dictionary<string, int>()
        {
            {"3",15 }, {"3+",40}, {"4",60}, {"4+",200}, {"5",850}, {"5+",7000}, {"6",0}, {"6+",0}
        };

        
    }
}
